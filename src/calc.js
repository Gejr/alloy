'use strict'
const csv = require('csvtojson');
let jsonFile;
let keys;
let sheetNumber = 1;
let oldSheetLength;
let newSheetLength;
let ranValues = {};
let x = 0;
let newJsonFile;
let fullJsonFile;

const form = document.querySelector('form');
form.addEventListener('submit', submitForm);

function submitForm(e) {
  e.preventDefault();
  const sheetLength = document.querySelector('#length').value;
  const file = document.querySelector('#file_input_file').files[0].path;
  csv().fromFile(file).then((jsonObj) => {
    jsonFile = jsonObj;
    calculateSheets(jsonFile, sheetLength);
  });
}

function calculateSheets(jsonFile, sheetLength) {
  jsonFile.sort(sort_by('length', true, parseInt));
  // console.log(JSON.stringify(jsonFile));
  var largestLength = jsonFile[0].length;

  // Take quantity and make it individual keys
  newJsonFile = `[`;
  for (var i = 0; i < jsonFile.length; i++) {
    let quan = jsonFile[i].quantity;
    for (var j = 0; j < quan; j++) {
      // x++;
      newJsonFile = newJsonFile.concat(`{"id":${jsonFile[i].id},"length":${jsonFile[i].length}},`);
    }
  }
  newJsonFile = newJsonFile.substring(0, newJsonFile.length-1);
  newJsonFile = newJsonFile.concat(`]`);
  console.log(newJsonFile);
  jsonFile = JSON.parse(newJsonFile);

  if(parseInt(largestLength) > parseInt(sheetLength)) {
    console.log(`Error, Sheet Length shorter than the largest subject. The largest subject is ${largestLength}mm, the sheet length is only ${sheetLength}mm`);
  } else {
    loader.classList.remove('invisible');
    keys = Object.keys(jsonFile);
    // console.log(keys);
    oldSheetLength = sheetLength;
    newSheetLength = sheetLength;
    var i = 0;
    fullJsonFile = `[`;
    countSheets(i, jsonFile, keys, newSheetLength, oldSheetLength);
  }
}

function render(fullJsonFile, sheetNumber) {
  fullJsonFile = fullJsonFile.substring(0, fullJsonFile.length-1);
  fullJsonFile = fullJsonFile.concat(`]`);
  fullJsonFile = JSON.parse(fullJsonFile);
  ipcRenderer.send('item:add', fullJsonFile, sheetNumber);
}

function countSheets(i, jsonFile, keys, newSheetLength, oldSheetLength) {
  // console.log(`Variables Check :: i = ${i} :: jsonFile = ${jsonFile} :: keys = ${keys} :: newSheetLength = ${newSheetLength} :: oldSheetLength = ${oldSheetLength}`);
  // console.log('countSheets start');
  // console.log(ranValues.length-1 == keys.length-1);
  // console.log(`ranValues length ${Object.size(ranValues)}    keys length ${keys.length-1}`);
  if(Object.size(ranValues) == keys.length) {
    render(fullJsonFile, sheetNumber);
  }
  else {
    if (i <= keys.length-1) {
      if(Object.values(ranValues).indexOf(i) > -1) {
        // console.log(`ranValues contains ${i}!`);
        i++;
        // x++;
        countSheets(i, jsonFile, keys, newSheetLength, oldSheetLength);

      } else {
        // console.log(`ranValues does not contain ${i}!`);
        if(jsonFile[i].length <= newSheetLength) {
          newSheetLength = newSheetLength - jsonFile[i].length;
          // console.log(`Subject ${i} (${jsonFile[i].length}) fits on sheet ${sheetNumber}; left on sheet ${sheetNumber} is ${newSheetLength}mm`);
          fullJsonFile = fullJsonFile.concat(`{"id":${jsonFile[i].id},"length":${jsonFile[i].length},"sheet":${sheetNumber}},`);
          ranValues[parseInt(i)] = parseInt(i);
          i++;
          countSheets(i, jsonFile, keys, newSheetLength, oldSheetLength);
        } else {
          // console.log(`Not enough space on sheet ${sheetNumber} (${newSheetLength}) for subject ${i}`);
          i++;
          countSheets(i, jsonFile, keys, newSheetLength, oldSheetLength);
        }
      }
    }
    else {
      i=0;
      sheetNumber++;
      newSheetLength = oldSheetLength;
      // console.log(oldSheetLength);
      // console.log(`STARTING OVER NEW SHEET LENGTH IS :: ${newSheetLength}`);
      countSheets(i, jsonFile, keys, newSheetLength, oldSheetLength);
    }
  }
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

var sort_by = function(field, reverse, primer){
  var key = primer ? function(x) {return primer(x[field])} : function(x) {return x[field]};
  reverse = !reverse ? 1 : -1;

  return function (a, b) {
    return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
  }
}
