'use strict'
const snackbarContainer = document.querySelector('#updateAnnouncer');

ipcRenderer.on('update:info', (e, msg) => {
  let data = {message: msg};
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
});
