'use strict'
const electron = require('electron');
const { ipcRenderer } = electron;
const table = document.querySelector('table');
const loader = document.getElementById('loader');
let json, sheet;

ipcRenderer.send('report:request');

ipcRenderer.on('report:full', (e, fullJsonFile, sheetNumber) => {
  json = fullJsonFile;
  sheet = sheetNumber;
  populateTable();
});

function removeLoad() {
  loader.classList.add('invisible');
}

function populateTable() {
  var caption = table.createCaption();
  var tHead = table.createTHead();
  var tHeadRow = table.insertRow(0);
  var tHeadCellId = tHeadRow.insertCell(0);
  var tHeadCellLength = tHeadRow.insertCell(1);
  var tHeadCellSheet = tHeadRow.insertCell(2);

  caption.innerHTML = "<h1>Full Report</h1>";
  tHeadCellId.innerHTML = "<b>Subject Id</b>";
  tHeadCellLength.innerHTML = "<b>Length</b>";
  tHeadCellSheet.innerHTML = "<b>Sheet Number</b>";

  for (var i = 0; i < json.length; i++) {
    let row = table.insertRow(-1);
    let cellId = row.insertCell(0);
    let cellLength = row.insertCell(1);
    let cellSheet = row.insertCell(2);

    cellId.innerHTML = json[i].id;
    cellLength.innerHTML = json[i].length;
    cellSheet.innerHTML = json[i].sheet;

    // Insert hint every nth time
    // if (i && (i % 30 === 0)) {
    //   let rowHint = table.insertRow(-1);
    //   let cellIdHint = rowHint.insertCell(0);
    //   let cellLengthHint = rowHint.insertCell(1);
    //   let cellSheetHint = rowHint.insertCell(2);
    //
    //   cellIdHint.innerHTML = "<b>Subject Id</b>";;
    //   cellLengthHint.innerHTML = "<b>Length</b>";
    //   cellSheetHint.innerHTML = "<b>Sheet Number</b>";
    // }
  }

  removeLoad();
}
