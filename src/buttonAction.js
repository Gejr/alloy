'use strict'
const {getCurrentWindow} = require('electron').remote;
const resetBtn = document.getElementById('resetBtn');
const reportBtn = document.getElementById('fullReport');
resetBtn.addEventListener('click', resetForm);
reportBtn.addEventListener('click', createReportWindow);

function resetForm() {
  getCurrentWindow().reload()
}

function createReportWindow() {
  createWindow('reportWin');
}

function createWindow(win) {
  ipcRenderer.send('btn:createWindow', win);
}
