'use strict'
const electron = require('electron');
const { ipcRenderer } = electron;
const loader = document.getElementById('loader');
const resPre = document.getElementById('resultPre');
const resNum = document.getElementById('resultNum');
const resPro = document.getElementById('resultPro');
const showMore = document.getElementById('showMore');
const fileUpload = document.getElementById('file_input_file');
const fileUpload2 = document.getElementById('FileUpload');
const calcButton = document.getElementById('Calculate');
const lengthInput = document.getElementById('length');

ipcRenderer.on('item:add', (e, fullJsonFile, sheetNumber) => {
  console.log('ipc');
  // console.log(JSON.stringify(fullJsonFile));
  const resultNumber = document.createTextNode(sheetNumber);
  resPre.textContent = `You will need`;
  resNum.textContent = `${sheetNumber}`;
  resPro.textContent = `sheets to fit all subjects`;
  fileUpload.disabled = true;
  fileUpload2.classList.add('mdl-button--raised[disabled]');
  fileUpload2.classList.add('mdl-button--raised');
  fileUpload2.classList.add('mdl-button--disabled');
  calcButton.disabled = true;
  lengthInput.disabled = true;
  showMore.classList.remove('invisible');
  loader.classList.add('invisible');
});
