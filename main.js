'use strict'
const electron = require('electron');
const url = require('url');
const path = require('path');
const { app, BrowserWindow, Menu, ipcMain } = electron;
const log = require('electron-log');
const {autoUpdater} = require("electron-updater");

let mainWindow, reportWindow, helpWindow;
let fullJsonFile, sheetNumber;

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';

log.info('App starting...');
// Listen for the app to be ready
app.on('ready', () => {
  mainWindow = new BrowserWindow({
    width: 800,
    minWidth: 500,
    height: 600,
    minHeight:400,
    title: 'Alloy',
  });

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'main.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Quit app when closed
  mainWindow.on('closed', () => {
    app.quit();
  });

  // Build menu from template
  const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);

  // Insert menu
  Menu.setApplicationMenu(mainMenu);
  if (process.env.NODE_ENV !== 'development') {
    setTimeout(function () {
      autoUpdater.checkForUpdates();
    }, 1000);
  }

});

// Handle create add window
function createReportWindow() {
  if(reportWindow == null) {
    reportWindow = new BrowserWindow({
      width: 800,
      minWidth: 500,
      height: 600,
      minHeight:400,
      title: 'Full Report',
    });

    reportWindow.loadURL(url.format({
      pathname: path.join(__dirname, '/src/report.html'),
      protocol: 'file:',
      slashes: true
    }));

    // Garbage collection handle
    reportWindow.on('close', () => {
      reportWindow = null;
    });
  }
  else {
    reportWindow.focus();
  }
}

function createHelpWindow() {
  if(helpWindow == null) {
    helpWindow = new BrowserWindow({
      width: 800,
      minWidth: 500,
      height: 600,
      minHeight:400,
      title: 'Help',
    });

    helpWindow.loadURL(url.format({
      pathname: path.join(__dirname, '/src/help.html'),
      protocol: 'file:',
      slashes: true
    }));

    // Garbage collection handle
    helpWindow.on('close', () => {
      helpWindow = null;
    });
  }
  else {
    helpWindow.focus();
  }
}

// Catch item:add
ipcMain.on('item:add', function (e, _fullJsonFile, _sheetNumber) {
  fullJsonFile = _fullJsonFile;
  sheetNumber = _sheetNumber;
  mainWindow.webContents.send('item:add', fullJsonFile, sheetNumber);
});

ipcMain.on('btn:createWindow', function (e, win) {
  switch (win) {
    case 'reportWin':
      createReportWindow();
      break;
    case 'helpWin':
      console.log('helpWin');
      createHelpWindow();
      break;
  }
});

ipcMain.on('report:request', function (e) {
  reportWindow.webContents.send('report:full', fullJsonFile, sheetNumber);
});

ipcMain.on('update:info', function (e, msg) {
  mainWindow.focus();
  mainWindow.webContents.send('update:info', msg);

})


// Create menu template
const mainMenuTemplate = [
  {
    label: 'File',
    submenu: [
      {
        label: 'Quit',
        accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
        click() {
          app.quit();
        },
      }
    ],
  },
  {
    label: 'Help',
    accelerator: process.platform == 'darwin' ? 'Command+H' : 'Ctrl+H',
    click() {
      createHelpWindow();
    },
  }
];

// If mac, add empty object to menu
if(process.platform == ' darwin') {
  mainMenuTemplate.unshift({});
}

// Add developer tools item if not in production
if(process.env.NODE_ENV == 'development') {
  mainMenuTemplate.push({
    label: 'Developer Tools',
    submenu: [
      {
        label: 'Toggle DevTools',
        accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: 'reload'
      }
    ]
  });
}

//
// Auto Updater
//

const sendStatusToWindow = (text) => {
  console.log(text);
  if(mainWindow) {
    mainWindow.focus();
    mainWindow.webContents.send('update:info', text);
  }
};

autoUpdater.on('checking-for-update', () => {
  sendStatusToWindow('Checking for update...');
});

autoUpdater.on('update-available', (info) => {
  sendStatusToWindow('Update available');
});

autoUpdater.on('update-not-available', (info) => {
  sendStatusToWindow('Update not available');
});

autoUpdater.on('error', (err) => {
  sendStatusToWindow(`Error in auto update: ${err.toString()}`);
});

autoUpdater.on('download-progress', (progressObj) => {
  sendStatusToWindow(`Download speed: ${progressObj.bytesPerSecond} - Downloaded ${progressObj.percent}%`);
});

autoUpdater.on('update-downloaded', (info) => {
  sendStatusToWindow('Update downloaded; will install now');
  autoUpdater.quitAndInstall();
});
